(()=>{

	let socket = io.connect("http://127.0.0.1:3000");

	angular.module('testReal')

	.controller('testController', [ "Comunications", "$scope", (Comunications, $scope)=>{

		$scope.functions = [];
		$scope.comunications = Comunications.get();
		$scope.comunication = {};


		//*****Eventos de socket.io*****
		//-- Eventos de escucha

		//Escucha del servidor cuando envia los Comunicados
		socket.on("comunicateViews", (data)=>{	
			Comunications.setGroup(data);
			$scope.$apply();
		})

		//Escucha cuando el servidor guardo un nuevo Comunicado
		socket.on('saveSuccess', (data)=>{
			Comunications.set(data);
			$scope.$apply();
		})

		//--Eventos de envio

		//*****Funciones de Angulara.Js*****

		//Funcion para comunicar al servidor cuando se guardo un nuevo Comunicado
		$scope.functions.sendComunicate = ()=>{
			let data = $scope.comunication;
			let fecha = new Date();
			data.date = fecha;
			socket.emit('save', data);
		}

		//Limpia los campos del formulario
		$scope.functions.limpiar = ()=>{
			$scope.comunication = {};
		}
	}])

})();