(()=>{

	// Servicio PM2 OJO
	//Se cargan las librerias correspondientes;
	//--Libreria http
	let http = require('http');
	//--Puerto del servidor
	let port = 3000;
	//--Dominio del servidor
	let ip = "netic.groupnet.es";
	//-- Framework de NodeJs
	let express = require('express');
	//-- Libreria Socket.io para tiempo real
	let socketio = require('socket.io');

	//Inicializando Express
	let app = express();

	app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.header("Access-Control-Allow-Headers", "Content-Type");
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        next();
    });

	//Se crea el servervidor
	let server = http.createServer(app);
	//Se escucha al servidor desde socket para el tiempo real
	let io = socketio.listen(server,{origins:'*:*'});

	//Detecta si se conecto el Cliente Socket.io 
	io.on('connection',(socket) => {
		//Detecta si el Cliente de Socket manda la orden 'saveData' para llamar a 'showData' del Cliente
		socket.on('saveData', ()=>{
			setInterval(()=>{io.sockets.emit('showData')}, 5000);
		})
	});

	//Se inicia el servidor de NodeJs
	server.listen(process.env.PORT || port, process.env.IP || ip, ()=>{
		let addr = server.address();
		console.log("Server Socket.io Listener at",  `${addr.address}:${addr.port}`);
	});
})();
