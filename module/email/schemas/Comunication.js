(()=>{
	let Sequelize = require('sequelize');
	let path = global.path;
	let db = require("../../../dbConfig");

	let ComunicationSchema = db.define('comunication', {
      user_id: Sequelize.INTEGER,
      department_id: Sequelize.INTEGER,
      date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
      title: Sequelize.TEXT,
      content: Sequelize.TEXT,
      acceptsReplies: Sequelize.BOOLEAN,
      type: Sequelize.INTEGER
    });

    module.exports = ComunicationSchema;

})();