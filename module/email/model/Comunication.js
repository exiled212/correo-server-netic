(()=>{

	let Q = require("q");
	let cacheManager = require("cache-manager");
	let memoryCache = cacheManager.caching({store:"memory", max:100, ttl:20})
	let schema = require("../schemas/Comunication");

	let db = require("../../../dbConfig.js");

    let idCache = "comunications";
    let idCacheGroup = "comunicationsGroup";

    const _getCache = Symbol('getCache');
    const _getComunications = Symbol('getComunications');

	class Comunication {

		constructor(data){
		}

		//***Metodos privados***

		//Metodo para tomar la cache
		[_getCache](id){
			let deferred = Q.defer();
			memoryCache.get(id, (err, result)=>{
				(err)? deferred.reject(new Error(err)) : deferred.resolve(result);
			});
			return deferred.promise;
		}

		[_getComunications](data){
			let deferred = Q.defer();
			if(data){
				deferred.resolve(data);
			} else {
				db.query("SELECT c.*, count(v.id) as views, u.firstname as firstname, u.lastname as lastname, u.avatar as avatar, count(a.attachment_id) as attachments "
			        +"FROM comunication as c "
			        +"left join views as v on c.id = v.message_id "
			        +"left join fos_user as u on c.user_id = u.id "
			        +"left join comunication_attachments as a on a.comunication_id = c.id "
			        +"group by c.id;", { type: db.QueryTypes.SELECT})
		        .then((result)=>{
	          		memoryCache.set(idCache, result, {ttl:20}, (err)=>{
			            (err)? deferred.reject(new Error(err)) : deferred.resolve(result);
	          		})
		        })
			} 
			return deferred.promise;
		}

		//Metodos Publicos
		getAll(){
			return this[_getCache](idCache)
			.then((result)=>{
				return this[_getComunications](result);
			})
			.catch((err)=>{
				console.log(err);
				throw err;
			})
		}

		getAllNotCache(){
			return this[_getCache](idCache)
			.then((result)=>{
				return this[_getComunications](result);
			})
			.catch((err)=>{
				console.log(err);
				throw err;
			})
		}


		getAllGroup(){
			return this[_getCache](idCacheGroup)
			.then((result)=>{
				return this[_getComunications](result);
			})
			.catch((err)=>{
				console.log(err);
				throw err;
			})
		}

		set(data){
			/*
		      INSERT INTO admin_netic.comunication (
		        user_id, 
		        department_id,
		        date, 
		        title, 
		        content, 
		        acceptsReplies, 
		        type
		      ) 
		      VALUES (13, now(), 'Test socket.io', 'Test desde socket.io', 0, 1);
		    */
			let deferred = Q.defer();
			schema.sync({
				force: true
			})
			.then(()=>{
				let comunication = schema.create(data);
				//comunication.save();
				deferred.resolve(data);
			})
			return deferred.promise;
		}

	}

	module.exports = Comunication;

})();