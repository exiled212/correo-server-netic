(()=>{
	let Sequelize = require('sequelize');
	let path = global.path;
	let config = require("./parameters");
	
	let sequelize = new Sequelize(config.db.name, config.db.user, config.db.password, {
	  host: config.db.host,
	  dialect: 'mysql'
	});

	module.exports = sequelize;
})();